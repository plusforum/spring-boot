package com.example.backend.service;

import com.example.backend.entity.RoomDao;
import com.example.backend.entity.StoreyDao;
import com.example.backend.repository.BuildingRepository;
import com.example.backend.repository.RoomRepository;
import com.example.backend.repository.StoreyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;

@Service
public class StoreyServiceImpl implements StoreyService{

    @Autowired
    StoreyRepository storeyRepository;
    @Autowired
    BuildingRepository buildingRepository;
    @Autowired
    RoomRepository roomRepository;

    @Override
    public List<StoreyDao> getAllStoreys() {
        List<StoreyDao> list = storeyRepository.findAll();
        if (!list.isEmpty()) {
            return list;
        }
        throw new NoSuchElementException();
    }

    @Override
    public StoreyDao getStoreyById(UUID uuid) {
        return storeyRepository.findById(uuid).orElseThrow();
    }

    @Override
    public StoreyDao addNewStorey(String name, UUID building_id) {
        buildingRepository.findById(building_id).orElseThrow();
        return storeyRepository.save(new StoreyDao(name, building_id));
    }

    @Override
    public StoreyDao updateStorey(UUID uuid, String name, UUID building_id) {
        buildingRepository.findById(building_id).orElseThrow();
        StoreyDao storey = storeyRepository.findById(uuid).orElseThrow();
        storey.setName(name);
        storey.setBuilding_id(building_id);
        return storeyRepository.save(storey);
    }

    @Override
    public boolean deleteStorey(UUID uuid) {
        for (RoomDao r : roomRepository.findAll()) {
            if (r.getStorey_id().equals(uuid)) {
                throw new IllegalCallerException();
            }
        }
        getStoreyById(uuid);
        storeyRepository.deleteById(uuid);
        return true;
    }
}
