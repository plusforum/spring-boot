package com.example.backend.service;

import com.example.backend.entity.RoomDao;

import java.util.List;
import java.util.UUID;

public interface RoomService {

    List<RoomDao> getAllRooms();

    RoomDao getRoomById(UUID uuid);

    RoomDao addNewRoom(String name, UUID storey_id);

    RoomDao updateRoom(UUID uuid, String name, UUID storey_id);

    boolean deleteRoom(UUID uuid);

}
