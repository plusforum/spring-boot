package com.example.backend.interceptor;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class Tracer implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {

        String traceHeader = request.getHeader("uber-trace-id");
        if (traceHeader != null) {
            response.setHeader("uber-trace-id", traceHeader);
        }
        return true;

    }

}
