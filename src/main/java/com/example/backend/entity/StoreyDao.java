package com.example.backend.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

import javax.persistence.*;
import java.util.UUID;

@Data
@NoArgsConstructor
@Entity
@EnableAutoConfiguration
@Table(name = "storeys")
public class StoreyDao {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    private String name;

    private UUID building_id;

    public StoreyDao(String name, UUID building_id) {
        this.name = name;
        this.building_id = building_id;
    }

    public StoreyDto toDto() {
        return new StoreyDto(name);
    }

}
