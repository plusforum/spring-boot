package com.example.backend.controller;

import com.example.backend.entity.StoreyDao;
import com.example.backend.request.AddStoreyRequest;
import com.example.backend.request.UpdateStoreyRequest;
import com.example.backend.service.StoreyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/assets/storeys/")
public class StoreyController {

    @Autowired
    StoreyService storeyService;

    @GetMapping()
    public ResponseEntity<List<StoreyDao>> getAllStoreys() {
        try {
            System.out.println("Try getting all storeys...");
            return new ResponseEntity<>(storeyService.getAllStoreys(), HttpStatus.OK);
        } catch (NoSuchElementException e) {
            System.out.println("No storeys found");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            System.out.println("Server Error");
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/{Id}")
    public ResponseEntity<StoreyDao> getStoreyById(@PathVariable UUID Id) {
        try {
            System.out.println("Try getting storey...");
            return new ResponseEntity<>(storeyService.getStoreyById(Id), HttpStatus.OK);
        } catch (NoSuchElementException e) {
            System.out.println("Storey not found");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            System.out.println("Server Error");
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping()
    public ResponseEntity<StoreyDao> addNewStorey(@RequestBody AddStoreyRequest addStoreyRequest) {
        try {
            System.out.println("Try adding new storey...");
            return new ResponseEntity<>(storeyService.addNewStorey(
                    addStoreyRequest.getName(),
                    addStoreyRequest.getBuilding_id()),
                    HttpStatus.OK);
        } catch (NoSuchElementException e) {
            System.out.println("Building not found");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            System.out.println("Server Error");
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/{Id}")
    public ResponseEntity<StoreyDao> updateStorey(@PathVariable UUID Id, @RequestBody UpdateStoreyRequest updateStoreyRequest) {
        UUID bodyId = updateStoreyRequest.getId();
        if (bodyId != null && !bodyId.equals(Id)) {
            System.out.println("StoreyId in URL and Body not equal");
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        try {
            System.out.println("Try updating storey...");
            return new ResponseEntity<>(storeyService.updateStorey(
                    Id,
                    updateStoreyRequest.getName(),
                    updateStoreyRequest.getBuilding_id()),
                    HttpStatus.OK);
        } catch (NoSuchElementException e) {
            System.out.println("Building or storey not found");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            System.out.println("Server Error");
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/{Id}")
    public ResponseEntity<Boolean> deleteStorey(@PathVariable UUID Id) {
        try {
            System.out.println("Try deleting storey...");
            return new ResponseEntity<>(storeyService.deleteStorey(Id), HttpStatus.OK);
        } catch (IllegalCallerException e) {
            System.out.println("Not deleted. There are rooms on that storey");
            return new ResponseEntity<>(HttpStatus.UNAVAILABLE_FOR_LEGAL_REASONS);
        } catch (NoSuchElementException e) {
            System.out.println("Storey not found");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            System.out.println("Server Error");
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}